package ru.omsu.imit;

import java.util.Objects;

public class Trainee {
    private String name;
    private String surname;
    private int mark;

    public Trainee(String name, String surname, int mark) throws TraineeException {
        if ( name.equals("")) {
            throw new TraineeException(TraineeErrorCodes.EMPTY_NAME_EXCEPTION.getErrorCode());
        }
        if(name == null){
            throw new TraineeException(TraineeErrorCodes.NULL_NAME_EXCEPTION.getErrorCode());
        }
        if ( surname.equals("")) {
            throw new TraineeException(TraineeErrorCodes.EMPTY_SURNAME_EXCEPTION.getErrorCode());
        }
        if(surname==null){
            throw new TraineeException(TraineeErrorCodes.NULL_SURNAME_EXCEPTION.getErrorCode());
        }
        if (mark < 1 || mark > 5) {
            throw new TraineeException(TraineeErrorCodes.WRONG_MARK_EXCEPTION.getErrorCode());
        }
        this.name = name;
        this.surname = surname;
        this.mark = mark;
    }

    public Trainee(Trainee object){
        this.name = object.getName();
        this.surname = object.getSurname();
        this.mark = object.getMark();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Trainee)) return false;
        Trainee trainee = (Trainee) o;
        return getMark() == trainee.getMark() &&
                getName().equals(trainee.getName()) &&
                getSurname().equals(trainee.getSurname());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getSurname(), getMark());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) throws TraineeException {
        if(name == null){
            throw new TraineeException(TraineeErrorCodes.NULL_NAME_EXCEPTION.getErrorCode());
        }
        if(name.equals("")){
            throw new TraineeException(TraineeErrorCodes.EMPTY_NAME_EXCEPTION.getErrorCode());
        }
        this.name = name;
    }

    public int getMark() {
        return mark;
    }

    public void setMark(int mark) throws TraineeException{
        if(mark < 1 || mark > 5){
            throw new TraineeException(TraineeErrorCodes.WRONG_MARK_EXCEPTION.getErrorCode());
        }
        this.mark = mark;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) throws TraineeException {
        if(surname == null){
            throw new TraineeException(TraineeErrorCodes.NULL_SURNAME_EXCEPTION.getErrorCode());
        }
        if(surname.equals("")){
            throw new TraineeException(TraineeErrorCodes.EMPTY_SURNAME_EXCEPTION.getErrorCode());
        }
        this.surname = surname;
    }
}
