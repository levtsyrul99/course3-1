package ru.omsu.imit;

public class TraineeException extends RuntimeException {

    public TraineeException() {
        super();
    }

    public TraineeException(String message) {
        super(message);
    }

    public TraineeException(String message, Throwable cause) {
        super(message, cause);
    }

    public TraineeException(Throwable cause) {
        super(cause);
    }
}



