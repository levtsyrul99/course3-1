package ru.omsu.imit;

public enum TraineeErrorCodes {
    EMPTY_NAME_EXCEPTION("Emty name exception"),
    NULL_NAME_EXCEPTION("Null name exception"),
    EMPTY_SURNAME_EXCEPTION("Empty surname exception"),
    NULL_SURNAME_EXCEPTION("Null surname exception"),
    WRONG_MARK_EXCEPTION("Wrong mark exception");

    private String errorCode;

    private TraineeErrorCodes(String errorCode){
        this.errorCode = errorCode;
    }

    public String getErrorCode(){
        return errorCode;
    }
}
